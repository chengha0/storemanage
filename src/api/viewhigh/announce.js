import request from '@/utils/request'

//查询
export function getGoodsList(data) {
  debugger;
  return request({
    url: '/bussiness/mateSaasBulletin/list',
    method: 'get',
    params: data
  })
};
//保存
export function updateAnnoAnnounce(data,edit) {
  debugger;
  return request({
    url: '/bussiness/mateSaasBulletin',
    method: edit?'put':'post',
    data: data
  })
};
//修改
export function editAnnoAnnounce(id) {
  return request({
    url: '/bussiness/mateSaasBulletin/'+id,
    method: 'get'
  })
};
//删除
export function deleteAnnoAnnounce(id) {
  return request({
    url: '/bussiness/mateSaasBulletin/'+id,
    method: 'delete'
  })
};
//发送
export function sendAnnoAnnounce(data) {
  return request({
    url: '/bussiness/mateSaasBulletin/batchCheckBulletin',
    method: 'put',
    data: data
  })
};
