import request from '@/utils/request'

// 入库保存 主表和明细
export function saveCheckGoods(data) {
  return request({
    url: '/bussiness/mateSaasWhrInHead/saveMateSaasWhrIn',
    method: 'post',
    data: data
  })
}

// 新增物资调用接口
export function saveGoods(data) {
  return request({
    url: '/bussiness/mateSaasInvDict',
    method: 'post',
    data: data
  })
}


//查询入库单明细
export function getGoodsDetails(data) {
    return request({
      url: '/bussiness/mateSaasWhrInHead/queryMateSaasWhrInDetail',
      method: 'get',
      params: data
    })
}

//查询入库主列表
export function getGoodsList(data) {
    return request({
      url: '/bussiness/mateSaasWhrInHead/list',
      method: 'get',
      params: data
    })
  }

//导入物资接口
export function importGoodsList(data) {
  return request({
    url: '/bussiness/mateSaasInvDict/list',
    method: 'get',
    params: data
  })
}
//入库物资明细
export function importGoodsDetails(billId) {
  return request({
    url: '/bussiness/mateSaasWhrInHead/queryMateSaasWhrInDetail/'+billId,
    method: 'get'
  })
}

//删除明细
export function deleteDetailsBill(billBids) {
  return request({
    url: '/bussiness/mateSaasWhrInLine/'+billBids,
    method: 'delete'
  })
}


export function deleteBill(billBids) {
  return request({
    url: '/bussiness/mateSaasWhrInHead/'+billBids,
    method: 'delete'
  })
}