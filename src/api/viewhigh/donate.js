import request from '@/utils/request'


export function getDonate(data) {
  return request({
    url: '/bussiness/persent/list',
    method: 'get',
    params: data
  })
}

//主表明细
export function getDonateDetail(billId) {
    return request({
      url: '/bussiness/persent/queryMateSaasPersentMainDetail/'+billId,
      method: 'get'
    })
  }

//确定
export function comfireForm(data) {
    return request({
      url: '/bussiness/persent/updatePersentInfo',
      method: 'post',
      data:data
    })
  }

//取消

export function cancelForm(data) {
    return request({
      url: '/bussiness/persent/updateCanclePersentInfo',
      method: 'post',
      data:data
    })
  }

