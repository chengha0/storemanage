import request from '@/utils/request'

// 出库引入保存
export function saveOutGoods(data) {
  return request({
    url: '/bussiness/mateSaasWhrOutHead/saveMateSaasWhrOutMainDetail',
    method: 'post',
    data: data
  })
}

//出库引入库
export function getGoodsforImport(data) {
    return request({
      url: '/bussiness/mateSaasWhrInLine/selectMateSaasWhrInForOutDtoList ',
      method: 'get',
      params: data
    })
}

//出库主列表
export function getOutGoodsList(data) {
    return request({
      url: '/bussiness/mateSaasWhrOutHead/list',
      method: 'get',
      params: data
    })
  }

//出库明细查询
export function getDetailsGoodsList(billId) {
  return request({
    url: '/bussiness/mateSaasWhrOutHead/queryMateSaasWhrOutMainAndDetail/'+ billId,
    method: 'get'
  })
}

//出库明细删除
export function deleteDetails(billBid) {
  return request({
    url: '/bussiness/mateSaasWhrOutLine/'+ billBid,
    method: 'delete'
  })
}

export function deleteOutBill(billBids) {
  return request({
    url: '/bussiness/mateSaasWhrOutHead/'+billBids,
    method: 'delete'
  })
}