import request from '@/utils/request'

// 查询订单列表
export function queryPurcOrder(data) {
  return request({
    url: '/bussiness/mateSaasOrderHead/list',
    method: 'get',
    data: data
  })
}

//同步订单至供应宝
export function syncOrderToGYB(data) {
    return request({
      url: '/bussiness/mateSaasOrderHead/syncOrder',
      method: 'put',
      params: data
    })
}

//删除订单
export function deleteOrder(billIds) {
  return request({
    url: '/bussiness/mateSaasOrderHead/' + billBids,
    method: 'delete',
    params: data
  })
}

//查看订单详情
export function viewOrder(billBids) {
  return request({
    url: '/bussiness/mateSaasOrderHead/'+ billBids,
    method: 'get'
  })
}

//查看订单明细详情
export function viewOrderDetail(billBids) {
  return request({
    url: '/bussiness/mateSaasOrderHead/'+ billBids,
    method: 'get'
  })
}

//保存订单
export function addOrder(data) {
  return request({
    url: '/bussiness/mateSaasOrderHead',
    method: 'post',
    params: data
  })
}

//获取仓库列表
export function getStoreList(data) {
  return request({
    url: '/bussiness/mateSaasStoreDict/queryAll',
    method: 'get',
    params: data
  })
}

